#!/bin/bash

set -e

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

buildbot-worker stop $SCRIPT_DIR/worker
buildbot stop $SCRIPT_DIR/master

buildbot start $SCRIPT_DIR/master
buildbot-worker start $SCRIPT_DIR/worker
