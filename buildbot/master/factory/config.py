import os

class Config:
	def __init__(self):
		# Web address (also used internally by buildbot) - this must be an actual IP, 'localhost' won't work
		self.http_url = '192.168.10.2'
		# Web port
		self.http_port = 8010
		self.web_title = 'godot c++ tmpl'
		self.web_title_url = 'https://gitlab.com/lyubomirv/godot-cpp-template'
		# Your project's git URL
		self.git_project_url = 'git@gitlab.com:lyubomirv/godot-cpp-template.git'
		# The directory with custom modules that will be compiled with the engine
		# Relative to the repository's root
		# Can be empty if no custom code needs to be built
		self.custom_modules_dir = 'src'
		# Main branch, usually 'master' or 'main'
		self.git_project_main_branch = 'main'
		# Whether to checkout the repository in a common place for all builders.
		# This is not a common thing to do but can help in case the project is
		# big and there is not much available disk space.
		# The checkout is done under buildbot/git
		# If a specific build is started manually (forced) and this is set to True,
		# there will be a checkbox in the force popup for selecting whether to use the
		# common checkout.
		self.git_use_combined_checkout = False
		# Branches to check for changes and build
		self.git_branches_to_poll = [ self.git_project_main_branch ]
		# Which platforms to build the editor for
		self.build_editor_android = False
		# NOTE: Building the web editor might result in an error. This
		# might be a local issue due to low memory or something else but still,
		# for now, disable it
		self.build_editor_web = False
		self.build_editor_linux = True
		self.build_editor_macos = True
		self.build_editor_windows = True
		# Whether to build x86/x86_64 editors - only relevant for Linux and Windows
		self.build_editor_x86 = True
		self.build_editor_x86_64 = True
		# Which platforms to build templates for (desktop templates are always built)
		self.build_templates_android = True
		self.build_templates_ios = True
		self.build_templates_web = True
		# While building the web editor/templates apparently there is a linking step
		# that is single-threaded and slow, so godot's build script by default starts
		# several processes with copies of the godot directory, so that it would be faster.
		# This may cause issues on less powerful systems or if low on space, so
		# this is a way to disable it.
		self.build_web_parallel = False
		# Standard build is with gdscript only
		self.build_standard = True
		# Mono build supports C#
		self.build_mono = True
		# Whether to export the game using the built editors and templates
		# This requires a project structure such as the one in:
		# https://gitlab.com/lyubomirv/godot-cpp-template
		# Note that either the standard or the mono editor/templates are
		# used for the export but not both. if build_standard is True, then
		# that is used, otherwise the mono builds are used.
		self.export_game = True
		# Godot's build scripts use podman by default,
		# although docker should work too
		self.container_runner = 'podman'
		# The version used when building the container images
		self.container_images_version = '4.x-f36'
		self.godot_build_name = 'custom_build'
		self.godot_version_status = 'stable'
		self.godot_branch_or_revision = '92bee43'
		self.godot_version = '4.0'
		# Threads available for building
		self.build_jobs = 8

		# The following settings are normally found in config.sh.in
		# if using the standard godot-build-scripts' build.sh/build-release.sh
		# ##################
		# This part is for signing the windows editor
		# Path to pkcs12 archive.
		self.sign_keystore = ''
		# Password for the private key.
		self.sign_password = ''
		# Name and URL of the signed application.
		# Use your own when making a thirdparty build.
		self.sign_name = ''
		self.sign_url = ''
		# ##################
		# This part is for signing the macos editor/templates, and
		# for exporting a game for ios (just the osx_host is needed for the export)
		# Hostname or IP address of an OSX host
		# eg "user@10.1.0.10"
		# An ssh key has to be set up for this, as the README explains
		self.osx_host = ''
		# ID of the Apple certificate used to sign
		self.osx_key_id = ''
		# Bundle id for the signed app
		self.osx_bundle_id = ''
		# Username/password for Apple's signing APIs (used for atltool)
		self.apple_id = ''
		self.apple_id_password = ''
		# Same as the osx_host above but used only when exporting for ios
		# It needs to be a different setting, though, as you could use on or the
		# other
		self.ios_export_osx_host = ''

		########################################################################
		# Do not modify, used internally
		self.root_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '../../..'))
		self.build_scripts_dir = os.path.join(self.root_dir, 'godot-build-scripts')
		self.buildbot_dir = os.path.normpath(os.path.join(self.root_dir, 'buildbot'))
		self.build_dir = os.path.join(self.buildbot_dir, 'build')
		self.latest_revisions_info_dir = os.path.join(self.build_dir, 'latest_revisions')
		self.deps_dir = os.path.join(self.root_dir, 'deps')
		self.vulkansdk_macos_dir = os.path.join(self.deps_dir, 'vulkansdk-macos/1.3.216.0')
		self.worker_name = 'worker'
		self.property_build_workdir = 'build_workdir'
		self.property_build_dir_for_revision = 'build_dir_for_revision'
		self.property_clean_build = 'clean_build'
		self.property_force_combined_checkout = 'force_combined_checkout'
		self.change_filter_project = 'godot-cpp'
		self.godot_buildbot_package_image = 'localhost/godot-buildbot-package:' + self.container_images_version
		self.godot_templates_version = self.godot_version + '.' + self.godot_version_status
