from buildbot.plugins import *

from .config import Config
from .util import checkout_git, create_factory, get_property_build_dir_for_revision, init_run_container_base_command_array

config = Config()

property_utility_revision = 'utility_revision'

def set_property_utility_revision(factory):
	command = [
		'cat',
		util.Interpolate(config.latest_revisions_info_dir + '/%(prop:branch:~' + config.git_project_main_branch + ')s.txt')]
	factory.addStep(
		steps.SetPropertyFromCommand(
			command = command,
			property = property_utility_revision,
			workdir = get_property_build_dir_for_revision(),
			description = 'setting property ' + property_utility_revision,
			descriptionDone = 'set property ' + property_utility_revision))

def get_import_assets_step(is_standard = True):
	# NOTE: Using 'timeout' is a workaround for the godot not currently having an
	# explicit parameter for generating the import cache. See the following link:
	# https://github.com/godotengine/godot/issues/69511#issuecomment-1335886507
	# If/when this is fixed, change this step.
	# As a matter of fact, this whole step would not be needed if godot 4 did
	# automatically create the import cache, like godot 3 did. If that happens,
	# then this whole step can be removed.
	command = init_run_container_base_command_array()
	command += [
		'-v', util.Interpolate(config.build_dir + '/%(prop:' + property_utility_revision + ')s/' + get_editor_binary_dir(is_standard) + ':/editor'),
		'-v', util.Interpolate('%(prop:' + config.property_build_workdir + ')s/project:/project'),
		'-w', '/project',
		config.godot_buildbot_package_image
	]
	command += [
		'timeout',
		'--preserve-status',
		'30s',
		'../editor/' + get_editor_binary(is_standard),
		'--headless',
		'--editor',
		'--verbose'
	]
	return steps.ShellCommand(
		command = command,
		haltOnFailure = False,
		flunkOnFailure = False,
		description = 'preparing asset import cache',
		descriptionDone = 'prepare asset import cache')

def get_create_dir_step(dir, desc_suffix = ''):
	return steps.MakeDirectory(
		dir = util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/' + dir),
		description = 'creating ' + (desc_suffix if desc_suffix else 'dir ' + dir),
		descriptionDone = 'create ' + (desc_suffix if desc_suffix else 'dir ' + dir))

def get_create_export_dir_step(dir):
	return get_create_dir_step('export/' + dir, 'export dir ' + dir)

def get_editor_binary_dir(is_standard):
	return 'out/linux/x86_64/tools/' if is_standard else 'out/linux/x86_64/tools-mono/'

def get_editor_binary(is_standard):
	return 'godot.linuxbsd.editor.x86_64' if is_standard else 'godot.linuxbsd.editor.x86_64.mono'

def get_templates_dir(is_standard):
	return 'tmp/templates/' if is_standard else 'tmp/mono/templates/'

def get_export_template_step(template, export_filename = '', is_standard = True, release = True, command_ext = []):
	command = init_run_container_base_command_array()
	command += command_ext
	command += [
		'-v', util.Interpolate(config.build_dir + '/%(prop:' + property_utility_revision + ')s/' + get_editor_binary_dir(is_standard) + ':/editor'),
		'-v', util.Interpolate(config.build_dir + '/%(prop:' + property_utility_revision + ')s/' + get_templates_dir(is_standard) + ':/root/.local/share/godot/export_templates/' + config.godot_templates_version),
		'-v', util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/export/' + template + ':/export'),
		'-v', util.Interpolate('%(prop:' + config.property_build_workdir + ')s/project:/project'),
		'-w', '/project',
		config.godot_buildbot_package_image
	]
	command += [
		'../editor/' + get_editor_binary(is_standard),
		'--headless',
		'--export-release' if release else '--export-debug',
		template,
		'../export/' + (export_filename if export_filename else template),
		'--verbose'
	]
	return steps.ShellCommand(
		command = command,
		haltOnFailure = True,
		description = 'exporting ' + template,
		descriptionDone = 'export ' + template)

def get_zip_exported_template_step(template):
	game_zip = template + '.zip'
	return steps.ShellCommand(
		command = ['zip', '-r', game_zip, template],
		workdir = util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/export'),
		haltOnFailure = True,
		description = 'zipping ' + template,
		descriptionDone = 'zip ' + template)

def get_copy_exported_to_game_step(template, game_zip_relative_to_export_dir):
	command = [
		'cp',
		'export/' + game_zip_relative_to_export_dir,
		'game' ]
	return steps.ShellCommand(
		command = command,
		workdir = get_property_build_dir_for_revision(),
		description = 'moving ' + template + ' to game dir',
		descriptionDone = 'move ' + template + ' to game dir')

def export_template(factory, template, export_filename = '', is_standard = True, zip = True, move = True, release = True, command_ext = []):
	factory.addStep(get_create_export_dir_step(template))
	factory.addStep(get_export_template_step(template, export_filename, is_standard, release, command_ext))
	if zip:
		factory.addStep(get_zip_exported_template_step(template))
	if move:
		factory.addStep(get_copy_exported_to_game_step(template, template + '.zip'))

def export_template_android(factory, is_standard):
	command_ext = [
		# The -4 at the end of the filename is the major version of godot
		'-v', config.buildbot_dir + '/editor_settings.tres:/root/.config/godot/editor_settings-4.tres'
	]

	# These actually combine two architectures each:
	#  - arm combines armv7 and arm64
	#  - x86 combines x86 and x86_64
	templates = ['android.arm', 'android.x86']
	for template in templates:
		output_filename_debug = template + '.debug.apk'
		output_filename_release = template + '.release.apk'
		export_template(factory, template, output_filename_debug, is_standard, False, False, False, command_ext)
		export_template(factory, template, output_filename_release, is_standard, False, False, True, command_ext)
		factory.addStep(get_copy_exported_to_game_step(template, template + '/' + output_filename_debug))
		factory.addStep(get_copy_exported_to_game_step(template, template + '/' + output_filename_debug + '.idsig'))
		factory.addStep(get_copy_exported_to_game_step(template, template + '/' + output_filename_release))
		factory.addStep(get_copy_exported_to_game_step(template, template + '/' + output_filename_release + '.idsig'))

def run_remote_macos_command(factory, command, desc, desc_done):
	full_command = init_run_container_base_command_array()
	full_command += [
		'-v', util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/export:/export'),
		'-w', '/export',
		config.godot_buildbot_package_image
	]
	full_command += command
	factory.addStep(
		steps.ShellCommand(
			command = full_command,
			haltOnFailure = True,
			description = desc,
			descriptionDone = desc_done))

def run_ssh_command(factory, command, desc, desc_done):
	ssh_command = [ 'ssh', config.ios_export_osx_host ]
	run_remote_macos_command(factory, ssh_command + command, desc, desc_done)

def run_scp_upload_dir_command(factory, local_dir, remote_dir):
	scp_command = [
		'scp',
		'-r',
		local_dir,
		config.ios_export_osx_host + ':' + remote_dir]
	run_remote_macos_command(
		factory,
		scp_command,
		'uploading ' + local_dir + ' to macos',
		'upload ' + local_dir + ' to macos')

def run_scp_download_command(factory, filename, remote_dir, local_dir):
	scp_command = [
		'scp',
		config.ios_export_osx_host + ':' + remote_dir + '/' + filename,
		local_dir]
	run_remote_macos_command(
		factory,
		scp_command,
		'downloading ' + filename + ' from macos',
		'download ' + filename + ' from macos')

def export_template_ios(factory, is_standard):
	export_template(factory, 'ios', '', is_standard, False, False)
	if not config.ios_export_osx_host:
		return

	remote_godot_export_ios_dir = '/tmp/godot-export-ios'
	prepare_dir_command = [ 'rm -rf ' + remote_godot_export_ios_dir + ' && mkdir ' + remote_godot_export_ios_dir ]
	run_ssh_command(factory, prepare_dir_command, 'preparing export dir on macos', 'prepare export dir on macos')
	run_scp_upload_dir_command(factory, 'ios', remote_godot_export_ios_dir)

	# The following is not tested, and the resulting filename might be incorrect or
	# there could be another error
	remote_build_dir = remote_godot_export_ios_dir + '/ios'
	xcodebuild_command = [
		'cd ' + remote_build_dir + ' && xcodebuild -verbose -project ios.xcodeproj -target ios -configuration Debug'
	]
	run_ssh_command(factory, xcodebuild_command, 'building ios on macos', 'build ios on macos')
	run_scp_download_command(factory, 'ios.ipa', remote_build_dir, '.')

	factory.addStep(get_copy_exported_to_game_step('ios', 'ios/ios.ipa'))

def export_template_macos(factory, is_standard):
	template = 'macos'
	output_filename = template + '.zip'
	export_template(factory, template, output_filename, is_standard, False, False)
	factory.addStep(get_copy_exported_to_game_step(template, template + '/' + output_filename))

def export(factory, is_standard, checkout = False):
	if checkout:
		checkout_git(factory, False)
	factory.addStep(get_create_export_dir_step(''))
	factory.addStep(get_create_dir_step('game'))
	set_property_utility_revision(factory)
	factory.addStep(get_import_assets_step(is_standard))
	export_template(factory, 'linux.x86_64', '', is_standard)
	export_template(factory, 'linux.x86', '', is_standard)
	export_template(factory, 'windows.x86_64', 'windows.x86_64.exe', is_standard)
	export_template(factory, 'windows.x86', 'windows.x86.exe', is_standard)
	export_template_macos(factory, is_standard)
	if config.build_templates_android:
		export_template_android(factory, is_standard)
	if config.build_templates_ios:
		export_template_ios(factory, is_standard)
	if config.build_templates_web:
		export_template(factory, 'web', 'web.html', is_standard)

def export_builder(is_standard):
	factory = create_factory()
	export(factory, is_standard, True)
	return factory
