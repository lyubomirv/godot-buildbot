import os
from buildbot.plugins import schedulers, util

from .config import Config
from .builder_config import BuilderConfig

config = Config()
builder_config = BuilderConfig()

def get_force_properties():
	properties = [
		util.BooleanParameter(
			name = config.property_clean_build,
			label = 'Clean build',
			default = False)]

	if config.git_use_combined_checkout:
		properties.append(
			util.BooleanParameter(
			name = config.property_force_combined_checkout,
			label = 'Use the combined git checkout',
			default = True))

	return properties

def should_export_only(props):
	for f in props.files:
		if f.startswith('godot') or f.startswith('src'):
			return False
	
	branch = props.getProperty('branch')
	branch = branch if branch else config.git_project_main_branch
	filename = os.path.join(config.latest_revisions_info_dir, branch + '.txt')
	if not os.path.exists(filename):
		return False

	return True

@util.renderer
def get_builders_any(props):
	if should_export_only(props):
		return builder_config.export_builders()
	return builder_config.any_branch_builders()

@util.renderer
def get_builders_nightly(props):
	if should_export_only(props):
		return builder_config.export_builders()
	return builder_config.nightly_builders()

def add_any_branch_scheduler(schedulers_array):
	scheduler = schedulers.AnyBranchScheduler(
		name = 'any_branch',
		treeStableTimer = 2 * 60,
		builderNames = get_builders_any)
	schedulers_array.append(scheduler)

def add_nightly_scheduler(schedulers_array):
	scheduler = schedulers.Nightly(
		name = 'nightly',
		change_filter = util.ChangeFilter(project = config.change_filter_project, branch = config.git_project_main_branch),
		onlyIfChanged  = True,
		hour = 2,
		builderNames = get_builders_nightly)
	schedulers_array.append(scheduler)

def add_force_shedulers(schedulers_array):
	schedulers_array.append(
		schedulers.ForceScheduler(
			name = 'force',
			builderNames = builder_config.force_builders(),
			properties = get_force_properties()))

def get_schedulers():
	schedulers_array = []

	add_any_branch_scheduler(schedulers_array)
	add_nightly_scheduler(schedulers_array)
	add_force_shedulers(schedulers_array)
	
	return schedulers_array
