from .config import Config
from .export import export
from .util import archive_godot, checkout_git, create_factory, get_build_release_step, get_build_step, store_revision_info

config = Config()

def build_builder(build_editor = False, package = False, should_export = True):
	factory = create_factory()
	checkout_git(factory)
	if config.build_mono:
		factory.addStep(get_build_step('godot-linux', 'build-mono-glue'))
	archive_godot(factory)
	if config.build_templates_android:
		factory.addStep(get_build_step('godot-android', 'build-android', 'android', build_editor and config.build_editor_android))
	if config.build_templates_ios:
		factory.addStep(get_build_step('godot-ios', 'build-ios', 'ios'))
	if config.build_templates_web:
		factory.addStep(get_build_step('godot-web', 'build-web', 'web', build_editor and config.build_editor_web))
	factory.addStep(get_build_step('godot-linux', 'build-linux', 'linux', build_editor and config.build_editor_linux))
	factory.addStep(get_build_step('godot-osx', 'build-macos', 'macos', build_editor and config.build_editor_macos))
	factory.addStep(get_build_step('godot-windows', 'build-windows', 'windows', build_editor and config.build_editor_windows))
	if package:
		factory.addStep(get_build_release_step())
		if build_editor:
			store_revision_info(factory)
		if should_export and config.export_game:
			export(factory, config.build_standard, False)
	return factory
