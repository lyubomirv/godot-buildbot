from buildbot.plugins import *

from factory import build
from factory import export
from factory.config import Config

class BuilderConfig:
	def __init__(self):
		self._config = Config()

	def any_branch_builders(self):
		builders = []
		builders.append(self._TEMPLATES)
		return builders

	def force_builders(self):
		builders = []
		if self._config.build_editor_x86 or self._config.build_editor_x86_64:
			builders.append(self._ALL)
		builders.append(self._TEMPLATES)
		if self._config.export_game:
			builders.append(self._EXPORT_GAME)
		return builders

	def nightly_builders(self):
		builders = []
		if self._config.build_editor_x86 or self._config.build_editor_x86_64:
			builders.append(self._ALL)
		else:
			builders.append(self._TEMPLATES)
		return builders
	
	def export_builders(self):
		builders = []
		builders.append(self._EXPORT_GAME)
		return builders
	
	def get_builders(self):
		builders = []
		if self._config.build_editor_x86 or self._config.build_editor_x86_64:
			builders.append(
				util.BuilderConfig(
					name = self._ALL,
					workernames = [self._config.worker_name],
					factory = build.build_builder(True, True, True)))
		builders.append(
			util.BuilderConfig(
				name = self._TEMPLATES,
				workernames = [self._config.worker_name],
				factory = build.build_builder(False, False, False)))
		if self._config.export_game:
			builders.append(
				util.BuilderConfig(
					name = self._EXPORT_GAME,
					workernames = [self._config.worker_name],
					factory = export.export_builder(self._config.build_standard)))
		return builders


	_ALL = 'all'
	_TEMPLATES = 'templates'
	_EXPORT_GAME = 'export'
