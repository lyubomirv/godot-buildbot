import os

from buildbot.plugins import *

from .config import Config

config = Config()

@util.renderer
def get_build_workdir(props):
	builddir_prop = props.getProperty('builddir')
	local_builder_dir = os.path.normpath(os.path.join(builddir_prop, 'build'))

	if not config.git_use_combined_checkout:
		return local_builder_dir

	scheduler = props.getProperty('scheduler')
	if scheduler == 'force':
		use_combined = props.getProperty(config.property_force_combined_checkout)
		if not use_combined:
			return local_builder_dir

	base_dir = os.path.normpath(os.path.join(builddir_prop, '..', '..'))
	branch = props.getProperty('branch')
	branch = branch if branch else config.git_project_main_branch
	return os.path.normpath(os.path.join(base_dir, 'git', branch))

@util.renderer
def get_git_checkout_mode(props):
	clean_build_prop = props.getProperty(config.property_clean_build)
	if clean_build_prop:
		return 'full'
	return 'incremental'

def set_property_build_workdir(factory):
	factory.addStep(
		steps.SetProperty(
			property = config.property_build_workdir,
			value = get_build_workdir,
			description = 'setting property ' + config.property_build_workdir,
			descriptionDone = 'set property ' + config.property_build_workdir))

def set_property_got_revision(factory):
	command = ['git', 'log', '-1', '--pretty=format:%h']
	property_name = 'got_revision'
	factory.addStep(
		steps.SetPropertyFromCommand(
			command = command,
			property = property_name,
			workdir = get_property_build_workdir(),
			description = 'setting property ' + property_name,
			descriptionDone = 'set property ' + property_name))

def set_property_build_dir_for_revision(factory):
	factory.addStep(
		steps.SetProperty(
			property = config.property_build_dir_for_revision,
			value = util.Interpolate(config.build_dir + '/%(prop:got_revision)s'),
			description = 'setting property ' + config.property_build_dir_for_revision,
			descriptionDone = 'set property ' + config.property_build_dir_for_revision))

def get_property_build_workdir():
	return util.Property(config.property_build_workdir)

def get_property_build_dir_for_revision():
	return util.Property(config.property_build_dir_for_revision)

def get_property_clean_build():
	return util.Property(config.property_clean_build)

def get_property_got_revision():
	return util.Property('got_revision')

def create_factory():
	factory = util.BuildFactory()
	set_property_build_workdir(factory)
	return factory

def prepare_build_dir_for_revision(factory):
	factory.addStep(
		steps.MakeDirectory(
			dir = get_property_build_dir_for_revision(),
			description = 'creating build dir',
			descriptionDone = 'create build dir'))

	if config.build_mono:
		factory.addStep(
			steps.MakeDirectory(
				dir = util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/mono-glue'),
				description = 'creating dir mono-glue',
				descriptionDone = 'create dir mono-glue'))

	out_dirs = [
		'android',
		'ios',
		'web',
		'linux',
		'macos',
		'uwp',
		'windows'
	]
	for out_dir in out_dirs:
		factory.addStep(
			steps.MakeDirectory(
				dir = util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/out/' + out_dir),
				description = 'creating dir ' + out_dir,
				descriptionDone = 'create dir ' + out_dir))

def checkout_git(factory, submodules = True):
	factory.addStep(
		steps.Git(
			repourl = config.git_project_url,
			mode = get_git_checkout_mode,
			submodules = submodules,
			branch = util.Property('branch', default = config.git_project_main_branch),
			alwaysUseLatest = True,
			progress = True,
			workdir = get_property_build_workdir()))
	set_property_got_revision(factory)
	set_property_build_dir_for_revision(factory)
	prepare_build_dir_for_revision(factory)

def init_run_container_base_command_array():
	command = [
		config.container_runner,
		'run',
		'--rm',
		'--env', 'BUILD_NAME=' + config.godot_build_name,
		'--env', 'GODOT_VERSION_STATUS=' + config.godot_version_status,
		'--env', 'NUM_CORES=' + str(config.build_jobs),
		'--env', 'CLASSICAL=' + '1' if config.build_standard else '0',
		'--env', 'MONO=' + '1' if config.build_mono else '0',
		'-w', '/root/'
	]
	return command

def init_run_container_command_array():
	godot_dir = 'godot'
	host_godot_dir = os.path.join('.', godot_dir)
	container_godot_dir = '/root/' + godot_dir

	command = init_run_container_base_command_array()
	command += [
		'--env', 'PRESET_GODOT_DIR=' + container_godot_dir,
		'-v', host_godot_dir + ':' + container_godot_dir
	]
	if config.build_mono:
		command += [
			'-v', util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/mono-glue:/root/mono-glue')
		]
	if config.custom_modules_dir:
		host_custom_modules_dir = os.path.join('.', config.custom_modules_dir)
		container_custom_modules_dir = '/root/custom_modules'
		command += [
			'--env', 'CUSTOM_MODULES_DIR=' + container_custom_modules_dir,
			'-v', host_custom_modules_dir + ':' + container_custom_modules_dir
		]
	return command

def get_platform_container_image(variant):
	return 'localhost/' + variant + ':' + config.container_images_version

def get_archive_godot_step():
	command = init_run_container_base_command_array()
	command += [
		'-v', util.Interpolate('%(prop:' + config.property_build_workdir + ')s:/root/git'),
		'-w', '/root/git/godot',
		config.godot_buildbot_package_image,
		'sh', 'misc/scripts/make_tarball.sh', '-v', config.godot_build_name, '-g', config.godot_branch_or_revision
	]
	return steps.ShellCommand(
		command = command,
		workdir = get_property_build_workdir(),
		haltOnFailure = True,
		description = 'archiving godot',
		descriptionDone = 'archive godot')

def get_move_godot_archive_step():
	command = [
		'mv',
		util.Interpolate('%(prop:' + config.property_build_workdir + ')s/godot-' + config.godot_build_name + '.tar.gz'),
		get_property_build_dir_for_revision()
	]
	return steps.ShellCommand(
		command = command,
		workdir = get_property_build_workdir(),
		haltOnFailure = True,
		description = 'moving godot archive',
		descriptionDone = 'move godot archive')

def archive_godot(factory):
	factory.addStep(get_archive_godot_step())
	factory.addStep(get_move_godot_archive_step())

def get_build_step(image_variant, platform_script_dir, platform_out_dir = None, build_editor = False):
	host_common_scripts_dir = os.path.join(config.build_scripts_dir, 'common')
	host_build_script_dir = os.path.join(config.build_scripts_dir, platform_script_dir)

	image = get_platform_container_image(image_variant)
	command = init_run_container_command_array()
	command += [
		'--env', 'BUILD_EDITOR=' + '1' if build_editor else '0',
		'--env', 'BUILD_EDITOR_x86=' + '1' if build_editor and config.build_editor_x86 else '0',
		'--env', 'BUILD_EDITOR_x86_64=' + '1' if build_editor and config.build_editor_x86_64 else '0',
		'--env', 'BUILD_TEMPLATES=1',
		'--env', 'BUILD_WEB_PARALLEL=' + '1' if config.build_web_parallel else '0',
		'-v', host_common_scripts_dir + ':/root/common',
		'-v', host_build_script_dir + ':/root/build',
		'-v', config.vulkansdk_macos_dir + ':/root/vulkansdk'
	]
	if platform_out_dir:
		command += ['-v', util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s/out/' + platform_out_dir + ':/root/out')]
	command += [
		image,
		'bash', 'build/build.sh', '2>&1'
	]

	description_suffix = platform_out_dir if platform_out_dir else ''
	return steps.ShellCommand(
		command = command,
		workdir = get_property_build_workdir(),
		timeout = None,
		description = 'building ' + description_suffix,
		descriptionDone = 'build ' + description_suffix)

def get_build_release_step():
	command = init_run_container_base_command_array()
	if config.sign_keystore:
		command += [
			'--env', 'SIGN_KEYSTORE=' + config.sign_keystore,
			'--env', 'SIGN_PASSWORD=' + config.sign_password,
			'--env', 'SIGN_NAME=' + config.sign_name,
			'--env', 'SIGN_URL=' + config.sign_url
		]
	if config.osx_host:
		command += [
			'--env', 'OSX_HOST=' + config.osx_host,
			'--env', 'OSX_KEY_ID=' + config.osx_key_id,
			'--env', 'OSX_BUNDLE_ID=' + config.osx_bundle_id,
			'--env', 'APPLE_ID=' + config.apple_id,
			'--env', 'APPLE_ID_PASSWORD=' + config.apple_id_password
		]

	build_type = 'all' if config.build_standard and config.build_mono else 'classical' if config.build_standard else 'mono'
	command += [
		'-v', util.Interpolate('%(prop:' + config.property_build_dir_for_revision + ')s:/root/build'),
		'-v', util.Interpolate('%(prop:' + config.property_build_workdir + ')s/godot:/root/build/git'),
		'-v', config.build_scripts_dir + '/build-release.sh:/root/build/build-release.sh',
		# '-v', config.build_scripts_dir + '/osslsigncode:/root/build/osslsigncode',
		'-v', config.vulkansdk_macos_dir + ':/root/build/deps/vulkansdk-macos',
		'-v', config.buildbot_dir + '/godot-build-scripts-stub-config.sh:/root/build/config.sh',
		'-w', '/root/build',
		config.godot_buildbot_package_image,
		'bash', '/root/build/build-release.sh', '-v', config.godot_build_name, '-t', config.godot_templates_version, '-b', build_type
	]
	return steps.ShellCommand(
		command = command,
		workdir = get_property_build_workdir(),
		description = 'running build-release.sh',
		descriptionDone = 'run build-release.sh')

def store_revision_info(factory):
	factory.addStep(
		steps.MakeDirectory(
			dir = config.latest_revisions_info_dir,
			description = 'creating latest revisions dir',
			descriptionDone = 'create latest revisions dir'))

	command = init_run_container_base_command_array()
	command += [
		'-v', config.latest_revisions_info_dir + ':/root/info',
		'-w', '/root/info',
		config.godot_buildbot_package_image,
		'bash', '-c',
		util.Interpolate('echo %(prop:got_revision)s > %(prop:branch:~' + config.git_project_main_branch + ')s.txt')
	]
	factory.addStep(
		steps.ShellCommand(
			command = command,
			workdir = get_property_build_workdir(),
			description = 'storing revision info',
			descriptionDone = 'store revision info'))
