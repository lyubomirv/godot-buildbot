godot-buildbot
---

#### What is this

This is a buildbot setup for automatic building of the godot engine, its export templates, and even exporting a game. It is based on the official [godot-build-scripts](https://github.com/godotengine/godot-build-scripts). These are the official scripts used to build the engine and templates which can be downloaded from godot's website. However, since they are not flexible enough to be used for custom builds, this is rather based on a [fork of them](https://github.com/lyubomirv/godot-build-scripts). The fork makes only minimal changes to the scripts to enable building only part of the templates, etc. (like disable uwp or web builds, if you don't need them).

In order to not install everything for every platform, the (official) build scripts make use of `podman` (an alternative to `docker`) and run the builds in containers, prepared for every specific platform. These containers are built using another [godot repository](https://github.com/godotengine/build-containers) which is also added as a submodule here.

#### Why

The point of this is to automate the creation of custom builds of godot. This is extremely useful when you want to work on a game in C++, or create a custom module that you want built with the engine, or for any other case where you need to compile the engine with your own modifications. Normally, for that you will need to build the editor/templates for every platform (or the ones you care about) and, if you are making a game in C++ for example, you need to have that automated and running after every change you make.

#### How to start

[*Short version of the instruction can be found at the end of this section.*](#short-instructions)

The official build scripts are meant to work on **Linux**. The buildbot implementation here may support other platforms but has not been tested on anything else other than Linux. So even if you build the containers on a Linux machine, your best bet still is to use Linux for buildbot too. The godot repo recommends fedora but ubuntu should work too.

First, you will need to build the docker/podman images for all platforms. Follow the instructions in `build-containers` (or in the [godot repo](https://github.com/godotengine/build-containers)). Basically, you need to  install some things on your machine and run one script that does all the work and wait for a very long time. You'll need at least 100GB of free space for this build, so keep that in mind.

Building for MacOS and iOS requires the VulkanSDK for MacOS. It can be downloaded from the [official website](https://vulkan.lunarg.com/). It must be installed on an actual MacOS system (during the installation I picked all options except to install it in the system directories) and then the contents of the SDK must be moved in this repository - in the directory `deps/vulkansdk-macos/<version>`. Currently the version used is `1.3.216.0` (I tried `1.3.236.0` but it didn't work) and that is set up in `config.py`. If using a different version, make sure to change it there.

During the build, some commands need to be executed on a MacOS machine. This is required, for example, for signing the MacOS editor and templates. This is something that the official build scripts do, but they allow skipping it and that can be configured in the config file (explained below). Honestly, if you are building a custom version of Godot for your own use, you definitely do not need to sign it. However, if you also want to export a game for iOS, you will need to at least set up the MacOS. To do that, you'll need a MacOS machine (or an alternative solution) that allows logging in via ssh, and that has XCode and the certificates for your ios app installed. You will need to set up a ssh key for automatic logging on the MacOS machine. Once you have created the ssh key and set it up on the MacOS machine, check the files in `dockerfiles/ssh` and copy the data from your keys into those files. Also, if you are using a non-standard port to ssh into the MacOS machine, put that port in the file `dockerfiles/ssh/config`.

There is one more container image that needs to be built and it is part of this repository. It is needed to for running parts of the build (and for exporting a game), again, without installing additional dependencies on your machine. Its dockerfile is in the directory `dockerfiles`, along with a script for building it - `build.sh`. Just run the script. This must be done after the data in the `ssh` directory is set up.

After you build the container images, you'll need to setup buildbot. It requires python3 and a few pip packages:

    pip3 install buildbot buildbot-worker buildbow-www buildbot-waterfall_view buildbot-console_view buildbot-grid_view

On some systems, like Debian 11, `buildbot` and `buildbot-worker` cannot be installed in this way, as of the time of writing. In such cases you should instead install those via the package manager (e.g. `apt`) and then all the rest via `pip3`.

The configuration of buildbot assumes that your code is in a git reposotory. You'll need to create a ssh-key for the machine running buildbot and set that up in the repository, so that buildbot can clone your code and check for changes.

Then run this command to prepare the buildbot master (replacing `<master-dir>` with the `buildbot/master` dir where you checked out this repository):

    buildbot upgrade-master <master-dir>

You'll need to edit the file `buildbot/master/factory/config.py` and set up everything as needed for you project. Note that buildbot assumes you are using a project structure as the one in the [cpp-template repository](https://gitlab.com/lyubomirv/godot-cpp-template), which is pretty standard, if you want to program using C++, be it a game or another custom module.

And finally, you can start the buildbot master and server using the script `buildbot/restart.sh`. You will also need to make this script run when the machine starts. You can now open your browser on the `<machine's IP>:8010` and you should see buildbot running. You should see it build your code on every change and also at 4:00 every night (if there have been changes during the day).

The resulting builds can be found in `buildbot/build/<short revision hash>/`. The editors/templates would be in `releases` and the exported game (if you are exporting a game) - in `game`.

##### Short instructions
1. Follow the instructions in `build-containers `
2. Run `build-containers/build.sh` (it will tell you to download xcode, so do that)
3. After downloading xcode, run `build-containers/build.sh` for real
5. For building for MacOS and iOS, install the VulkanSDK on a MacOS machine and copy move it to `deps/vulkansdk-macos/<version>`
4. For signing on MacOS (or exporting for iOS), set up a MacOS machine for remote access via ssh and set up logging with a ssh key. Then copy the data of the ssh key to the files in `dockerfiles/ssh`
5. `> ./dockerfiles/build.sh`
6. `> pip3 install buildbot buildbot-worker buildbot-www buildbot-waterfall_view buildbot-console_view buildbot-grid_view`
7. `> buildbot upgrade-master buildbot/master`
8. Make changes in `buildbot/master/factory/config.py`
9. `> ./buildbot/restart.sh`
10. Make `buildbot/restart.sh` run on machine start

#### Other notes / known issues

- Signing windows executables is disabled at the moment. It needs `osslsigncode` from somewhere and this will be figured out later.
- Currently, the container image for building UWP is disabled in the official scripts, as there seems to be some issue. So, building for UWP is not possible at the moment. Note that even if the container image is fixed, UWP builds with Mono would still be unsupported - again, because that is not supported in the official build scripts.
- Currently, at least on my side, there is an issue building the web editor. Something during the build makes it crash and it doesn't seem related to buildbot or any of my code. It might also be an issue in the official scripts, or it might be a local issue on my side (like not enough availabe disk space). So it is supported by the buildbot configuration but is disabled in the `config.py`. You may have more luck with it.
- The iOS game export is not entirely tested. When exporting for iOS, the project is first exported by the editor, and then needs another run through xcode on a MacOS machine. This second part is not tested, as it requires an actuall App ID, etc.
