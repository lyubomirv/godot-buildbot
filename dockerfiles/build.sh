#!/bin/bash
set -e

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

podman=`which podman || true`

if [ -z $podman ]; then
  echo "podman needs to be in PATH for this script to work."
  exit 1
fi

img_version=4.x-f36

pushd ${SCRIPT_DIR}
$podman build -t godot-buildbot-package:${img_version} -f Dockerfile.godot-buildbot-package
popd
